package com.ServerApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientThread {
    private Socket socket = null ;
    public ClientThread (Socket socket) { this.socket = socket;}
    public void run () {
        try {
            //request de la client
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(socket.getOutputStream());

            //citim fiecare linie de la server
            String request = in.readLine();
            String raspuns;

            //while pana la "stop"
            while(!request.equalsIgnoreCase("stop")){
                //serverul  citeste requesturile si trimite raspuns
                System.out.println("Client request: " + request);
                raspuns = "Server response: " + request;
                out.println(raspuns);
                out.flush();

                //citim comenzile lui de la tastatura
                request = in.readLine();
            }
            //La "stop" serverul se opreste
            raspuns = "Server received stop!";
            System.out.println(raspuns);
            out.println(raspuns);
            out.flush();

            //the socket will close the communication with the client
        } catch (IOException e) {
            System.err.println("Communication error... " + e);
        } finally {
            try {
                socket.close(); // or use try-with-resources
            } catch (IOException e) { System.err.println (e); }
        }
    }
}