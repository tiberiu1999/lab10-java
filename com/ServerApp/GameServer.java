package com.ServerApp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {

    public static final int PORT = 8100;
    public boolean serverOn = true;
    public GameServer() throws IOException {
        ServerSocket serverSocket = null;
        try {
            //the server reads from the given port
            serverSocket = new ServerSocket(PORT);
            while (true) {
                System.out.println("Waiting for a client ...");
                Socket socket = serverSocket.accept();
                new ClientThread(socket).run();
                //daca comanda este "stop", se opreste
            }
        } catch (IOException e) {
            System.err.println("Ooops... " + e);
        } finally {
            serverSocket.close();
            System.out.println("Server is stopped");
        }
    }
}