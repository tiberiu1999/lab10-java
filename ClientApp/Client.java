package ClientApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        String serverAddress = "127.0.0.1";
        int PORT = 8100;
        try (
                //se creeaza un nou socket.
                Socket socket = new Socket(serverAddress, PORT);
                PrintWriter out =
                        new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()))) {

            //mesaj catre client
            System.out.println("Please write a command:");
            Scanner scanner = new Scanner(System.in);

            //citim de la taastatura cu scan
            String request = scanner.nextLine();
            String response;

            //facem un while pana primim "EXIT"
            while (!request.equalsIgnoreCase("exit")){

                //trimitem la server
                out.println(request);

                //primim raspunsulW
                response = in.readLine();
                System.out.println(response);
                System.out.println("\nPlease write another command:");

                //citim alte comenzi pana la "exit"
                request = scanner.nextLine();
            }
        } catch (UnknownHostException e) {
            System.err.println("No server listening... " + e);
        }
    }

}